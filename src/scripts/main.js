let user;
let userNameTBId = "userName";
let userAvatarUrlTBId = "userAvatarUrl";
let userStorageKey = "user";
let commentListStorageKey = "commentList";
// need to do a dfs traversal of comment tree
let rootCommentList = [];
let commentIdMap = {};
let timestampUpdateMS = 5000;
let defaultAvatarUrl = "https://thesocietypages.org/socimages/files/2009/05/nopic_192.gif";
let replyToImg = "https://cdn1.iconfinder.com/data/icons/smoothline-chat/30/chat_020-message_reply-arrow-conversation-email-512.png";

let replyModal = {
  parent: $id("container"),
  specialMessage: "Truth or dare?",
  visible: false,
  returnFocusTo: $id("newCommentText"),

  show(parentComment, specialMessage = this.specialMessage) {
    this.visible = true;
    this.parentComment = parentComment;
    this.parent.innerHTML = this.render(specialMessage);

    //get focus
    $id("replyCommentText").focus();

  },

  hide() {
    this.visible = false;
    this.parent.innerHTML = "";
    this.parentComment = null;
  },

  handleReply() {
    let textValue = $id("replyCommentText").value;

  
    if (this.parentComment) {
      let added = addComment(new CommentNode(user.name, user.avatarUrl, new Date(), textValue), this.parentComment);
      if (added) this.hide();
    }
  },

  render(specialMessage) {
    return `
    <div class="modal">
          <div class="modal__bg">
            <div class="modal__layout">
              <div class="modal__content">
                <div class="modal__special_message">
                  Writing reply to <b>${this.parentComment.name}</b> as <b>${user.name}</b>
                  <p>
                  On ${this.parentComment.time}, ${this.parentComment.name} said: 
                  </p>
                  <div class="quote_reply">
                    <i>${this.parentComment.text}</i>
                  </div>
                  
                </div>
                <input class="binput modal__input" style="width:50rem" 
                id="replyCommentText" placeholder="Add your response..." 
                onkeypress="event.keyCode === 13 && replyModal.handleReply()"/>
                <div class="modal__btns">
                  <button class="btn" onclick="replyModal.hide()">Cancel</button>
                  <button class="btn" onclick="replyModal.handleReply()">Reply</button>
                </div>  
              </div>
            </div>
          </div>
        </div>
    `;
  }

}

class User {
  constructor(name, avatarUrl) {
    this.name = name;
    this.avatarUrl = avatarUrl;
  }
}

//////////////// LOAD SAVED CONTEXT /////////////////
(
  () => {
    //get username from localStorage and load old comment list.
    let storedUser = localStorage.getItem("user");
    let littleDFSCrawlerForMappingIds = (commentNode) => {
      commentIdMap[commentNode.id] = commentNode;
      commentNode.children.forEach(child => littleDFSCrawlerForMappingIds(child));
    };
    if (storedUser) {
      user = Object.assign(new User("", ""), JSON.parse(storedUser));
    } else {
      user = new User("Harman Singh", "https://thesocietypages.org/socimages/files/2009/05/vimeo.jpg");
    }
    $id(userNameTBId).value = user.name;
    $id(userAvatarUrlTBId).value = user.avatarUrl;
    let storedCommentList = localStorage.getItem(commentListStorageKey);
    if (storedCommentList) {
      rootCommentList = JSON.parse(storedCommentList);
    }
    rootCommentList.forEach(commentNode => littleDFSCrawlerForMappingIds(commentNode));
    // hoisted :)
    render();
  }
)();



class CommentNode {
  constructor(name, avatarUrl, time, text) {
    this.name = name;
    this.avatarUrl = avatarUrl;
    this.time = time;
    this.text = text;
    this.votes = 0;
    this.children = [];
    this.id = null;
  }

  upvote() {
    this.votes++;
  }

  downvote() {
    this.votes--;
  }
}
////////////// EVENT HANDLERS ////////////
// open reply dialog for comment.
/**
 * 
 * @param {string} id of comment
 * @param {boolean} up upvote if true / downvote if false.
 */
function handleVotingGlobal(id, up) {
  if (up) {
    CommentNode.prototype.upvote.call(commentIdMap[id]);
  } else {
    CommentNode.prototype.downvote.call(commentIdMap[id]);
  }
  render();
  persist();
}

function handleReplyGlobal(id) {
  replyModal.show(commentIdMap[id]);
}

function addComment(commentNode, parentComment) {
  //generate id here
  if (commentNode && commentNode.text.trim().length === 0){
    alert("Please enter a valid comment");
    return false;
  }
  if (parentComment) {
    commentNode.id = parentComment.id.concat((parentComment.children.length + 1));
    parentComment.children.push(commentNode);
  } else {
    commentNode.id = '' + (rootCommentList.length + 1);
    rootCommentList.push(commentNode);
  }
  commentIdMap[commentNode.id] = commentNode;
  render();
  // save list in localStorage
  persist();
  return true;
};

// inefficient persistance but theres no API, so will stick with this for now
function persist() {
  localStorage.setItem(commentListStorageKey, JSON.stringify(rootCommentList));
}

// renders the comment List, links all html to cmnt_container
function render() {
  let rootElement = document.getElementById("cmnt_container");
  rootElement.innerHTML = "";
  combinedHTML = "";
  let renderTime = new Date();
  rootCommentList.forEach(commentNode => combinedHTML = combinedHTML.concat(renderNodeDFSish(commentNode, 0, null, renderTime)));
  rootElement.innerHTML = combinedHTML;
};

/**
 * should return html of the whole comment tree for 1 parent comment
 * 
 * @param  CommentNode commentNode
 * @param  int depth
 */
function renderNodeDFSish(commentNode, depth, parentNode, renderTime = new Date()) {
  if (!(commentNode.time instanceof Date)){
    commentNode.time = new Date(commentNode.time);
  }
  let returnStr = renderCommentTemplate(commentNode, depth, parentNode, renderTime);
  return returnStr.concat(
    commentNode.children.map(
      childComment => renderNodeDFSish(childComment, depth + 1, commentNode, renderTime)
    ).join('')
  );
};


//inner html
// todo : move to class
function renderCommentTemplate(commentNode, depth, parentNode, renderTime = new Date()) {
  return (`
  <div class="cmnt" style="margin-left:${depth * 3}rem">
      <!-- Avatar (LHS) -->
      <div>
        <img class="cmnt__avatar" src="${commentNode.avatarUrl || defaultAvatarUrl}"/>
      </div>
      <!-- Rest of the body (RHS) -->
      <div class="cmnt__body">
        <!-- Top row contains Name + timing information -->
        <div class="cmnt__top_row">
          <div class="cmnt__handle">
            ${commentNode.name}
          </div>
          <span class="dot mx-2"></span>
          ${
            depth > 0 && parentNode? `<img class="reply_to" src="${replyToImg}"/>
            <div class="reply_to">${parentNode.name}</div>
            <span class="dot mx-2"></span>` : ''
          }
          
          <div class="cmnt__time">
            ${getTimeDifferenceInString(commentNode.time, renderTime)}
          </div>
        </div>
        <!-- The text body of comment -->
        <div class="cmnt__text">${commentNode.text}</div>
        <!-- Bottom Row of a comment -->
        <div class="cmnt__bottom_row">
          <div class="vote_count">${commentNode.votes}</div>
          <span class="dot mx-2"> </span>
          <div class="cmnt__vote_button clickable" onclick="handleVotingGlobal(${commentNode.id}, true)">Like</div>
          <span class="dot mx-2"> </span>
          <div class="cmnt__vote_button clickable" onclick="handleVotingGlobal(${commentNode.id}, false)">Dislike</div>
          <span class="dot mx-2"> </span>
          <div class="cmnt__reply" onclick="handleReplyGlobal(${commentNode.id})">Reply</div>
          <span class="dot mx-2"> </span>
          <div class="cmnt__share">Share</div>
        </div>

      </div>
    </div>
    `);
};
let comment1 = new CommentNode("Harman Singh", null, new Date(), "I think its a great comment widget");
// let comment2 = new CommentNode("Harman Pring", null, new Date(), "I dont ");
// let comment3 = new CommentNode("Neeti Dhanak", null, new Date(), "Why not???");
// let comment4 = new CommentNode("Steve bobs", null, new Date(), "You must be stupid");
// addComment(comment1);
// addComment(comment2, comment1);
// addComment(comment3, comment2);
// addComment(comment4, comment2);

// Event binding
(() => {
  let idNewCommentButton = "addNewComment";
  let newCommentButton = $id(idNewCommentButton);
  let newCommentTextBox = $id("newCommentText");
  let handlerForNewComment = () => {
    addComment(new CommentNode(user.name, user.avatarUrl, new Date(), newCommentTextBox.value, null));
    newCommentTextBox.value = "";
    newCommentTextBox.setAttribute("placeholder", "Your comment has been posted");
    setTimeout(() => {
      newCommentTextBox.setAttribute("placeholder", "Add more to the discussion...");
    }, 4500);
  };
  newCommentButton.addEventListener("click", handlerForNewComment);
  newCommentTextBox.addEventListener("keypress", event => event.keyCode === 13 && handlerForNewComment());

  let idSaveUserInfo = "btnSaveUserInfo";
  document.getElementById(idSaveUserInfo).addEventListener(
    "click",
    () => {
      let newUserName = $id(userNameTBId).value;
      let newUserAvatarUrl = $id(userAvatarUrlTBId).value;

      let newUser = new User(newUserName, newUserAvatarUrl.trim().length > 0 ? newUserAvatarUrl : null);
      localStorage.setItem("user", JSON.stringify(newUser));
      //update global user
      user = newUser;
    });
})();

function $id(id) {
  return document.getElementById(id);
}

function getTimeDifferenceInString(fromDate, currentDate) {
  let ms = currentDate - fromDate;
  let sec = ms / 1000;
  let pluralAdder = (x) => Math.round(x) > 1 ? 's' : '';
  if (sec < 60) {
    return `${Math.round(sec)} second${pluralAdder(sec)} ago`;
  }
  let min = sec / 60;
  if (min < 60) {
    return `${Math.round(min)} minute${pluralAdder(min)} ago`;
  }
  let hr = min / 60;
  if (hr < 24) {
    return `${Math.round(hr)} hour${pluralAdder(hr)} ago`;
  }
  let day = hr / 24;
  return `${Math.round(day)} day${pluralAdder(day)} ago`;
}

///////////// UPDATE TIMINGS ////////////////
timestampUpdater = () => {
  render();
  setTimeout(timestampUpdater, timestampUpdateMS);
}
timestampUpdater();