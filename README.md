# Comment widget
This widget was tested on chrome browser, macOS High Sierra. Uses es6 syntaxes.

## What this widget supports : 
1. Adding new comments as a particular user
2. Changing current user's name and avatarUrl, fallback to default avatar if no url is provided. Sample urls to try: 
    - https://s18429.pcdn.co/wp-content/uploads/2016/07/medium-default-avatar.png
    - https://thesocietypages.org/socimages/files/2009/05/vimeo.jpg
3. Adding replies to comments (nested comments till any depth)
4. Upvoting / Downvoting comments, and display their count
5. Display the time when comment was added, in FB-like timestamps (ex: 2 minutes ago...)
    - The page rerenders after every 5 seconds to update timestamps.
6. Using HTML tags to format comment body.
7. Adding comments/replies using `Enter` key

## What this widget does not support: 
1. Effecient re-rendering algorithms.
    - Since frameworks like react / angular were not used, the rerendering process is not very effecient as it has not been tweaked.